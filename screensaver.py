import sys
import pygame


pygame.init()

# Set screen size
screen = pygame.display.set_mode((720, 480))

# variables
pygame.display.set_caption("Umar Screensaver")

x = 90
y = 300
dx = 0.5
dy = 0.5


#screensaver loop
while True:
    # Event detection
    # This is necessary for being able to close the game
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()



    #update game loop
    x += dx
    y += dy
    
    #black background
    screen.fill((0,0,0))


     # Bounce the logo off the walls
    if x > 720 or x < 0:
        dx = -dx
    if y > 480 or y < 0:
        dy = -dy


    # my name as the logo
    font = pygame.font.Font(None, 50)
    text = font.render("Umar", True, (0, 0, 255))
    text_rect = text.get_rect()
    text_rect.center = (x,y)
    screen.blit(text, text_rect)


    # Update the screen
    pygame.display.update()

# Quit pygame
pygame.quit()
